import 'package:kinara/host/Url.dart';
import 'dart:convert';
import 'dart:async';
import 'package:http/http.dart'
as http;

Future ambilDataProfil(userId) async {
  http.Response hasil = await http.get(
    Uri.encodeFull("${baseurl}user/getUserById/" + userId.toString()), headers: {
      "Accept": "application/json"
    }
  );
  return json.decode(hasil.body)["data"]["user"][0];
}

Future ambilDataKeluarga(userId) async {
  http.Response hasilAmbilDataKeluarga = await http.post(
    Uri.encodeFull("${baseurl}keluarga/getKeluarga"), headers: {
      "Accept": "application/json"
    },
    body: {
      "user_id": userId.toString(),
    }
  );
  List dataKeluarga = json.decode(hasilAmbilDataKeluarga.body)['data']['keluarga'];
  return dataKeluarga;
}