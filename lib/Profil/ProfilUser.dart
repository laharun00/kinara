import 'dart:convert';
import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:kinara/host/Api.dart';

profilUser(lebarLayar, tinggiLayar, userId) {
  Uint8List bytes;
  return FutureBuilder < dynamic > (
    future: ambilDataProfil(userId),
    builder: (context, snapshot) {
      if (snapshot.hasData) {
        if (snapshot.data["user_image"] != null) {
          bytes = base64.decode(snapshot.data["user_image"]);
        }
        var userNotlp = (snapshot.data["user_notlp"] == TextRange.empty) ? '' : 'No Tlp : ' + snapshot.data["user_notlp"];
        return Container(
          width: lebarLayar,
          height: tinggiLayar,
          decoration: BoxDecoration(
            image: DecorationImage(
              fit: BoxFit.fitHeight,
              colorFilter: new ColorFilter.mode(Colors.black.withOpacity(0.3), BlendMode.dstATop),
              image:
              (snapshot.data["user_image"] != null) ?
              MemoryImage(bytes) :
              NetworkImage("https://3.bp.blogspot.com/-Kjvdq7atrkk/W-1a8fK11OI/AAAAAAAAAhk/BpTZWiLf6esJC60snzJbuXVIAzyBI49QgCHMYCw/s800/wallpaper-iphone-tumblr-hd-496-iphone-wallpaper.jpg"),
            ),
          ),
          alignment: AlignmentDirectional.center,
          child: Column(
            children: [
              Padding(padding: EdgeInsets.only(top: 30, bottom: 0, left: 60, right: 60)),
              Container(
                child: Container(
                  height: 150.0,
                  width: 150.0,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: Colors.green,
                    image: DecorationImage(
                      image:
                      (snapshot.data["user_image"] != null) ?
                      ResizeImage(MemoryImage(bytes, scale: 1.0), width: 150, height: 150) :
                      NetworkImage("https://cdn.shortpixel.ai/client/to_webp,q_glossy,ret_img/https://kinarababycare.com/wp-content/uploads/2020/04/kinara-baby-care-logo-landscape.png"),
                    )
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 30, bottom: 0, left: 60, right: 60),
                child: Text(
                  snapshot.data["user_nama"],
                  style: TextStyle(
                    fontWeight: FontWeight.bold, fontSize: 20, ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 20, bottom: 0, left: 30, right: 30),
                child: Text(
                  snapshot.data["user_alamat"] + '\n' + userNotlp,
                  style: TextStyle(
                    height: 1.5,
                    fontSize: 15,
                    fontStyle: FontStyle.normal,
                  ),
                ),
              ),
            ],
          ),
        );
      } else if (snapshot.hasError) {
        return Container(
          child: Text(snapshot.error),
        );
      } else {
        return Container(
          alignment: Alignment.center,
          child: Padding(
            padding: EdgeInsets.only(top: lebarLayar / 2),
            child: CircularProgressIndicator(
              value: null,
              strokeWidth: 2.0,
            ),
          )

        );
      }
    },
  );
}