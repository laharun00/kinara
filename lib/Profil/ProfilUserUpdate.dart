import 'dart:io';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:kinara/Treatment/TreatmentKategori.dart';
import 'package:kinara/host/Api.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:kinara/host/Url.dart';
import 'dart:convert';
import 'dart:async';
import 'dart:typed_data';
import 'package:http/http.dart'
as http;

// ignore: must_be_immutable
class ProfilUserUpdate extends StatefulWidget {
  var userId, userNama, userEmail, userAlamat, userNotlp, dataKeluarga;
  ProfilUserUpdate({
    this.userId,
    this.userNama,
    this.userEmail,
    this.userAlamat,
    this.userNotlp,
    this.dataKeluarga
  });
  @override
  _ProfilUserUpdateState createState() => _ProfilUserUpdateState(this.userId, this.userNama, this.userEmail, this.userAlamat, this.userNotlp, this.dataKeluarga);
}

class _ProfilUserUpdateState extends State < ProfilUserUpdate > {
  _ProfilUserUpdateState(this.userId, this.userNama, this.userEmail, this.userAlamat, this.userNotlp, this.dataKeluarga);
  final scaffoldProfilUserUpdate = GlobalKey < ScaffoldState > ();
  final _formProfilUserUpdate = GlobalKey < FormState > ();
  var userId, userNama, userEmail, userAlamat, userNotlp, dataKeluarga;
  var tUserNama = TextEditingController();
  var tUserEmail = TextEditingController();
  var tUserAlamat = TextEditingController();
  var tUserNotlp = TextEditingController();
  var tUserPasswordLama = TextEditingController();
  var tUserPasswordBaru = TextEditingController();
  var tUserupdatedAt = TextEditingController();
  var tUsercreatedAt = TextEditingController();
  var tUserPassword = TextEditingController();
  var lihatPassLama = true;
  var lihatPassBaru = true;
  var res;
  int iDataKeluarga = 0;
  File file;
  File inputFile;
  Uint8List bytes;

  @override
  Widget build(BuildContext context) {
    // var lebarLayar = MediaQuery.of(context).size.width;
    return WillPopScope(
      child: Scaffold(
        key: scaffoldProfilUserUpdate,
        appBar: AppBar(
          backgroundColor: Colors.white,
          leading: IconButton(
            color: Colors.blue[700],
            icon: Icon(Icons.arrow_back),
            onPressed: () {_tombolKembaliDitekan();}
          ),
          actions: [
            Padding(
              padding: EdgeInsets.only(right: 10),
              child: IconButton(
                icon: Icon(Icons.check,
                  color: Colors.blue[500],
                ),
                onPressed: () {
                  if (_formProfilUserUpdate.currentState.validate()) {
                    updateDataUser();
                  }
                }
              ),
            ),
          ],
        ),
        body: SingleChildScrollView(
          child: FutureBuilder < dynamic > (
            future: ambilDataProfil(userId),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                if (snapshot.data["user_image"] != null) {
                  bytes = base64.decode(snapshot.data["user_image"]);
                }
                tUserNama.text = snapshot.data["user_nama"];
                tUserEmail.text = snapshot.data["user_email"];
                tUserAlamat.text = snapshot.data["user_alamat"];
                tUserNotlp.text = snapshot.data["user_notlp"];
                tUserPassword.text = snapshot.data["user_password"];
                tUserupdatedAt.text = snapshot.data["updated_at"];
                tUsercreatedAt.text = snapshot.data["created_at"];
                return Container(
                  child: Padding(padding: EdgeInsets.only(left: 20.0, right: 20.0, top: 10.0, bottom: 30.0),
                    child: Column(
                      children: [
                        SizedBox(height: ScreenUtil.getInstance().setSp(50)),
                        Column(
                          children: [
                            FlatButton(
                              onPressed: pilihCaraUpload,
                              child: Stack(
                                children: [
                                  Container(
                                    child: Container(
                                      height: 120.0,
                                      width: 120.0,
                                      decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        color: Colors.green,
                                        image: DecorationImage(
                                          image:
                                          file != null ? ResizeImage(FileImage(file), width: 120, height: 120) :
                                          bytes != null ? ResizeImage(MemoryImage(bytes), width: 120, height: 120) :
                                          ResizeImage(NetworkImage("https://cdn.shortpixel.ai/client/to_webp,q_glossy,ret_img/https://kinarababycare.com/wp-content/uploads/2020/04/kinara-baby-care-logo-landscape.png"), width: 110, height: 50)
                                        )
                                      ),
                                    ),
                                  ),
                                  Positioned(
                                    top: 7.0,
                                    left: 90.0,
                                    child: Icon(Icons.photo_camera, size: 25, color: Colors.blue)
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),

                        Form(
                          key: _formProfilUserUpdate,
                          child: Column(
                            children: [
                              SizedBox(height: ScreenUtil.getInstance().setHeight(30)),
                              TextFormField(
                                style: TextStyle(color: Colors.black45),
                                decoration: InputDecoration(
                                  fillColor: Colors.red,
                                  labelText: "Email",
                                  hintStyle: TextStyle(color: Colors.grey, fontSize: 16.0),
                                  icon: Icon(Icons.email),
                                ),
                                keyboardType: TextInputType.emailAddress,
                                readOnly: true,
                                enabled: false,
                                controller: tUserEmail,
                                maxLengthEnforced: false,
                                validator: (value) {
                                  if (value.isEmpty) {
                                    return 'Email tidak boleh kosong';
                                  }
                                  return null;
                                },
                              ),
                              
                              SizedBox(height: ScreenUtil.getInstance().setHeight(30)),
                              TextFormField(
                                decoration: InputDecoration(
                                  labelText: 'Nama',
                                  hintStyle: TextStyle(color: Colors.grey, fontSize: 16.0),
                                  icon: Icon(Icons.person_pin)
                                ),
                                controller: tUserNama,
                                textCapitalization: TextCapitalization.words,
                                validator: (value) {
                                  if (value.isEmpty) {
                                    return 'Nama harus diisi';
                                  }
                                  return null;
                                },
                              ),

                              SizedBox(height: ScreenUtil.getInstance().setSp(30)),
                              TextFormField(
                                decoration: InputDecoration(
                                  labelText: "Nomor Telepon",
                                  hintStyle: TextStyle(color: Colors.grey, fontSize: 16.0),
                                  icon: Icon(Icons.phone),
                                ),
                                keyboardType: TextInputType.phone,
                                controller: tUserNotlp,
                                validator: (value) {
                                  if (value.isEmpty) {
                                    return 'Nomor Telepon tidak boleh kosong';
                                  }
                                  return null;
                                }
                              ),

                              SizedBox(height: ScreenUtil.getInstance().setHeight(30)),
                              TextFormField(
                                decoration: InputDecoration(
                                  labelText: "Alamat",
                                  hintStyle: TextStyle(color: Colors.grey, fontSize: 16.0),
                                  icon: Icon(Icons.map),
                                ),
                                controller: tUserAlamat,
                                keyboardType: TextInputType.multiline,
                                maxLines: 4,
                                textCapitalization: TextCapitalization.words,
                                validator: (value) {
                                  if (value.isEmpty) {
                                    return 'Alamat tidak boleh kosong';
                                  }
                                  return null;
                                },
                              ),

                              SizedBox(height: ScreenUtil.getInstance().setHeight(90)),
                              Padding(
                                padding: EdgeInsets.only(left: 40, bottom: 10),
                                child: Text("* Jika mengubah password maka, PASSWORD LAMA & BARU wajib di isi.\n* Jika tidak mengubah password maka, kosongi saja.",
                                  style: TextStyle(
                                    color: Colors.blue,
                                    height: 1.7
                                  ),
                                ),
                              ),

                              SizedBox(height: ScreenUtil.getInstance().setHeight(30)),
                              TextFormField(
                                decoration: InputDecoration(
                                  labelText: "Password Lama",
                                  hintStyle: TextStyle(color: Colors.grey, fontSize: 16.0),
                                  icon: Icon(Icons.keyboard),
                                  suffixIcon: IconButton(
                                    icon: Icon(Icons.remove_red_eye),
                                    onPressed: presLihatPassLama
                                  ),
                                ),
                                controller: tUserPasswordLama,
                                obscureText: lihatPassLama,
                              ),

                              SizedBox(height: ScreenUtil.getInstance().setHeight(30)),
                              TextFormField(
                                decoration: InputDecoration(
                                  labelText: "Password Baru",
                                  hintStyle: TextStyle(color: Colors.grey, fontSize: 16.0),
                                  icon: Icon(Icons.keyboard),
                                  suffixIcon: IconButton(
                                    icon: Icon(Icons.remove_red_eye),
                                    onPressed: presLihatPassBaru
                                  ),
                                ),
                                controller: tUserPasswordBaru,
                                obscureText: lihatPassBaru,
                              ),
                            ],
                          )
                        ),
                      ]
                    ),
                  )
                );
              } else {
                return Container(
                  alignment: Alignment.center,
                  child: Padding(
                    padding: EdgeInsets.only(top: 450 / 2),
                    child: CircularProgressIndicator(
                      value: null,
                      strokeWidth: 2.0,
                    ),
                  )
                );
              }
            }
          ),
        )
      ),
      onWillPop: () {_tombolKembaliDitekan(); return;}
    );

  }

  void pilihCaraUpload() {
    showDialog(
      context: context,
      child: AlertDialog(
        title: Text("Pilih sumber gambar"),
        actions: [
          MaterialButton(
            child: Column(
              children: [
                Icon(Icons.camera_alt),
                Text("Camera"),
              ],
            ),
            onPressed: () async {
              Navigator.pop(context);
              // ignore: deprecated_member_use
              inputFile = await ImagePicker.pickImage(source: ImageSource.camera);
              setState(() {
                file = inputFile;
              });
            },
          ),
          MaterialButton(
            child: Column(
              children: [
                Icon(Icons.photo_library),
                Text("Gallery"),
              ],
            ),
            onPressed: () async {
              Navigator.pop(context);
              // ignore: deprecated_member_use
              inputFile = await ImagePicker.pickImage(source: ImageSource.gallery);
              setState(() {
                file = inputFile;
              });
            },
          )
        ],
      )
    );
  }

  void presLihatPassLama() {
    setState(() {
      lihatPassLama = !lihatPassLama;
    });
  }

  void presLihatPassBaru() {
    setState(() {
      lihatPassBaru = !lihatPassBaru;
    });
  }

  Future < void > updateDataUser() async {
    String base64Image;
    String userImageNama;

    if (file != null) {
      base64Image = base64Encode(file.readAsBytesSync());
      userImageNama = file.path.split("/").last;
    } else {
      base64Image = '';
      userImageNama = '';
    }

    final response = await http.post("${baseurl}user/userUpdate", body: {
      "user_id": userId.toString(),
      "user_nama": tUserNama.value.text.toString(),
      "user_email": tUserEmail.value.text.toString(),
      "user_notlp": tUserNotlp.value.text.toString(),
      "user_alamat": tUserAlamat.value.text.toString(),
      "user_password_lama": tUserPasswordLama.value.text.toString(),
      "user_password_baru": tUserPasswordBaru.value.text.toString(),
      "user_image": base64Image.toString(),
      "user_image_nama": userImageNama.toString(),
    }, headers: {
      "Accept": "application/json"
    });
    setState(() {
      var convertDataToJson = json.decode(response.body);
      res = convertDataToJson['status'];
    });

    _displayNotifikasi(BuildContext context, _scaffoldKey, String pesan) {
      final snackBar = SnackBar(content: Text(pesan));
      _scaffoldKey.currentState.showSnackBar(snackBar);
    }

    if (res == 200) {
      _displayNotifikasi(context, scaffoldProfilUserUpdate, 'Data berhasil diubah.');
    } else if (res == 201) {
      _displayNotifikasi(context, scaffoldProfilUserUpdate, 'Tidak ada data yang diperbarui');
    } else {
      _displayNotifikasi(context, scaffoldProfilUserUpdate, 'Terjadi kesalahan saat menghubungkan ke server');
    }
  }

  void _tombolKembaliDitekan() {
    Navigator.pop(context, true);
    Navigator.pop(context, true);
    var route = new MaterialPageRoute(
      builder: (BuildContext context) => new TreatmentKategori(
        userId: userId,
        kembali: 3,
      ),
    );
    Navigator.of(context).push(route);
  }

}