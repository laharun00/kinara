import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
// import 'package:indonesia/indonesia.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:kinara/Treatment/FormDetailReservasi.dart';
import 'package:kinara/Treatment/FormJadwalReservasi.dart';
import 'package:kinara/Treatment/TreatmentKategori.dart';
import 'dart:convert';
import 'dart:async';
import 'package:kinara/host/Url.dart';
import 'package:http/http.dart'
as http;

// ignore: must_be_immutable
class Keranjang extends StatefulWidget {
  var kembali;
  int userId;
  var latitude;
  var longitude;
  var tempatLatiLong;

  Keranjang({
    this.userId,
    this.kembali,
    this.latitude,
    this.longitude,
    this.tempatLatiLong,
  });
  @override
  _KeranjangState createState() => _KeranjangState(
    this.userId,
    this.kembali,
    this.latitude,
    this.longitude,
    this.tempatLatiLong
  );
}

class _KeranjangState extends State < Keranjang > {
  final _scaffoldKey = GlobalKey < ScaffoldState > ();
  var latitude;
  var longitude;
  var tempatLatiLong;
  var kembali;
  int userId;
  List dataKeranjang;

  _KeranjangState(
    this.userId,
    this.kembali,
    this.latitude,
    this.longitude,
    this.tempatLatiLong
  );

  var _alamat = new TextEditingController();
  var _catatan = new TextEditingController();
  var tanggalTreatment = TextEditingController();
  final format = DateFormat("dd MMM, yyyy HH:mm");
  var maxTimeDate = new DateTime.now().add(new Duration(days: 50));

  _displayNotifikasi(BuildContext context, _scaffoldKey, String pesan) {
    final snackBar = SnackBar(content: Text(pesan));
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }

  Future < void > ambilDataKeranjang(userId) async {
    final response =
      await http.post("${baseurl}reservasi/keranjang", body: {
        "user_id": userId.toString(),
      });
    setState(() {
      var convertDataToJson = json.decode(response.body);
      dataKeranjang = convertDataToJson['data']['keranjang'];
    });
  }

  _hapusReservasi(reservasiId) async {
    final response =
      await http.post("${baseurl}reservasi/hapusReservasi", body: {
        "reservasi_id": reservasiId.toString(),
      });
    setState(() {
      var convertDataToJson = json.decode(response.body);
      if (convertDataToJson['status'] == 200) {
        _displayNotifikasi(context, _scaffoldKey, 'Treatment dihapus dari keranjang');
        ambilDataKeranjang(userId);
      } else {
        _displayNotifikasi(context, _scaffoldKey, 'Terjadi kesalahan');
      }
    });
  }

  _formDetailReservasi(userId, reservasiId, treatmentNama) {
    var route = new MaterialPageRoute(
      builder: (BuildContext context) => FormDetailReservasi(
        userId: userId,
        reservasiId: reservasiId,
        treatmentNama: treatmentNama,
      ),
    );
    Navigator.of(context).push(route);
  }

  @override
  // ignore: must_call_super
  void initState() {
    this.ambilDataKeranjang(userId);
    if(tempatLatiLong != null){
      _alamat.text = tempatLatiLong.toString();
    }
  }

  @override
  Widget build(BuildContext context) {

    // _alamat.text = "Jalan janti kanoman tegal pasar tr 08 rw 202 no 282";
    // _catatan.text = "Jalan janti ";
    return WillPopScope(
      child: Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          elevation: 0.1,
          centerTitle: true,
          title: const Text('Konfirmasi Pesanan', style: TextStyle(color: Colors.black)),
            backgroundColor: Colors.white,
            leading: IconButton(
              color: Colors.blue[700],
              icon: Icon(Icons.arrow_back),
              onPressed: () {
                if (kembali == null) {
                  Navigator.pop(context, true);
                  // Navigator.pop(context, true);
                } else {
                  _tombolKembaliDitekan(context);
                }
              }
            ),
            actions: [
              Padding(
                padding: EdgeInsets.only(right: 10),
                child: IconButton(
                  icon: Icon(Icons.playlist_add_check,
                    color: Colors.blue[500],
                  ),
                  onPressed: () {
                    konfirmasiPesanan();
                  }
                ),
              ),
            ],
        ),
        body: body(),
        bottomNavigationBar: BottomAppBar(
          shape: const CircularNotchedRectangle(),
            child: Container(height: 50.0, ),
        ),
        // floatingActionButton: FloatingActionButton.extended(
        //   onPressed: () {
        //     var route = new MaterialPageRoute(
        //       builder: (BuildContext context) => FormJadwalReservasi(
        //         userId: userId
        //       )
        //     );
        //     Navigator.of(context).push(route);
        //   },
        //   label: Text('Lanjutkan memesan'),
        //   icon: Icon(Icons.navigation),
        //   backgroundColor: Colors.pink,
        // ),
      ),
      onWillPop: () {
        if (kembali == null) {
          Navigator.pop(context, true);
          // Navigator.pop(context, true);
        } else {
          _tombolKembaliDitekan(context);
        }
        return;
      }
    );
  }

  void _tombolKembaliDitekan(context) {
    // Navigator.pop(context, true);
    Navigator.pop(context, true);
    var route = new MaterialPageRoute(
      builder: (BuildContext context) => new TreatmentKategori(
        userId: userId,
        kembali: 0,
      ),
    );
    Navigator.of(context).push(route);
  }

  body() {
    return Form(
      child: Column(
        children: [
          Padding(
            padding: EdgeInsets.only(left: 18.0, right: 30.0, top: 00.0, bottom: 0),
            child: Column(
              children: [
                TextField(
                  textCapitalization: TextCapitalization.words,
                  decoration: InputDecoration(
                    labelText: "Alamat",
                    hoverColor: Colors.green[400],
                    icon: Icon(Icons.map, color: Colors.green[400]),
                    hintStyle: TextStyle(color: Colors.green[400], fontSize: 6),
                    contentPadding: EdgeInsets.only(left: 11),
                    labelStyle: TextStyle(fontSize: 13),
                  ),
                  maxLines: 4,
                  controller: _alamat,
                  readOnly: true,
                  onTap: () {
                    var route = new MaterialPageRoute(
                      builder: (BuildContext context) => FormJadwalReservasi(
                        userId: userId
                      )
                    );
                    Navigator.of(context).push(route);
                  },
                ),

                Container(
                  height: 5,
                ),

                DateTimeField(
                  controller: tanggalTreatment,
                  format: format,
                  onShowPicker: (context, currentValue) async {
                    final ambilTanggal = await showDatePicker(
                      context: context,
                      firstDate: DateTime.now(),
                      initialDate: currentValue ?? DateTime.now(),
                      lastDate : maxTimeDate
                    );
                    if (ambilTanggal != null) {
                      final ambilWaktu = await showTimePicker(
                        context: context,
                        initialTime:
                        TimeOfDay.fromDateTime(currentValue ?? DateTime.now()),
                      );
                      return (DateTimeField.combine(ambilTanggal, ambilWaktu));
                    } else {
                      return currentValue;
                    }
                  },
                  decoration: InputDecoration(
                    icon: Icon(Icons.calendar_today, color: Colors.green[400]),
                    labelText: "Waktu & Tanggal Treatment",
                    contentPadding: EdgeInsets.only(left: 11, top: 8),
                    labelStyle: TextStyle(fontSize: 13),
                  ),
                ),

                Container(
                  height: 5,
                ),
                

                TextField(
                  decoration: InputDecoration(
                    labelText: "Catatan",
                    icon: Icon(Icons.note, color: Colors.green[400]),
                    hintStyle: TextStyle(color: Colors.grey, fontSize: 6),
                    contentPadding: EdgeInsets.only(left: 11),
                    labelStyle: TextStyle(fontSize: 13),
                  ),
                  controller: _catatan,
                ),

                Container(
                  height: 30,
                ),

              ],
            ),
          ),
          Expanded(
            child: ListView.separated(
              itemCount: dataKeranjang == null ? 0 : dataKeranjang.length,
              itemBuilder: (context, i) {
                return ListTile(
                  leading: IconButton(icon: Icon(Icons.close, color: Colors.red[400]), onPressed: () => _hapusReservasi(dataKeranjang[i]["reservasi_id"])),
                  title: Text(dataKeranjang[i]["treatment_nama"], style: TextStyle(height: 1.5)),
                  subtitle: Text(dataKeranjang[i]["keluarga_nama"] + "\n" + dataKeranjang[i]["bidan_nama"], style: TextStyle(height: 1.5)),
                  trailing: IconButton(icon: Icon(Icons.keyboard_arrow_right), onPressed: () => _formDetailReservasi(userId, dataKeranjang[i]["reservasi_id"], dataKeranjang[i]["treatment_nama"])),
                  contentPadding: EdgeInsets.only(left: 5.0, top: 0.0, bottom: 0.0, right: 25.0),
                );
              },
              separatorBuilder: (context, index) {
                return Divider();
              },
            )
          ),
        ],
      ),
    );
  }

  void konfirmasiPesanan() async {
    print(userId);
    print(tanggalTreatment);
    print(tempatLatiLong.toString());
    print(_catatan);
    print(latitude);
    print(longitude);
//    final respons = await http.post("${baseurl}reservasi/konfirmasiReserfasi",body: {
//      'userId': userId,
//      'tanggalJam': tanggalTreatment,
//      'alamat': _alamat,
//      'catatan': _catatan
//    });
  }
}