import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:kinara/Treatment/Keranjang.dart';
import 'dart:convert';
import 'dart:async';
import 'package:kinara/host/Url.dart';
import 'package:http/http.dart'
as http;

// ignore: must_be_immutable
class FormDetailReservasi extends StatefulWidget {
  FormDetailReservasi({
    this.userId,
    this.reservasiId,
    this.treatmentNama
  });
  int userId;
  int reservasiId;
  String treatmentNama;

  @override
  _FormDetailReservasiState createState() => _FormDetailReservasiState(this.userId, this.reservasiId, this.treatmentNama);
}

class _FormDetailReservasiState extends State < FormDetailReservasi > {
  _FormDetailReservasiState(this.userId, this.reservasiId, this.treatmentNama);
  final scaffoldFormDetailReservasi = GlobalKey < ScaffoldState > ();
  var res;
  int userId;
  int reservasiId;
  int _valBidan;
  int _valKeluarga;
  List dataBidan;
  List dataKeluarga;
  List dataReservasi;
  String treatmentNama;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldFormDetailReservasi,
      appBar: AppBar(
        backgroundColor: Colors.white,
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          color: Colors.blue[700],
          onPressed: () {
            kembaliKeKeranjang();
          }
        ),
        actions: [
          Padding(
            padding: EdgeInsets.only(right: 10),
            child: IconButton(
              icon: Icon(Icons.check,
                color: Colors.blue[500],
              ),
              onPressed: () {
                updateFormDetailReservasi();
              }
            ),
          ),
        ],
      ),
      body: bodyFormDetailReservasi(),
    );
  }

  bodyFormDetailReservasi() {
    return SingleChildScrollView(
      child: Container(
        child: Padding(padding: EdgeInsets.only(left: 20.0, right: 20.0, top: 30.0, bottom: 30.0),
          child: Column(
            children: < Widget > [
              Text(treatmentNama,
                style: TextStyle(
                  fontSize: ScreenUtil.getInstance().setSp(35),
                  fontFamily: "Poppins-Bold",
                  letterSpacing: .20)
              ),

              SizedBox(height: ScreenUtil.getInstance().setSp(50)),
              Row(
                children: < Widget > [
                  Icon(Icons.person_pin, color: Colors.grey, ),
                  Padding(padding: EdgeInsets.only(right: 15)),
                  DropdownButton(
                    hint: Text("Pilih Nama Anak"),
                    value: _valKeluarga,
                    items: dataKeluarga ?.map((item) {
                      return DropdownMenuItem(
                        child: Row(
                          children: < Widget > [
                            SizedBox(
                              width: 250,
                              child: Text(" " + item['keluarga_nama'],
                                textAlign: TextAlign.left,
                                style: new TextStyle(color: Colors.black54),
                              ),
                            ),
                          ],
                        ),
                        value: item['keluarga_id'],
                      );
                    }) ?.toList() ?? [],
                    onChanged : (value) {
                      setState(() {
                        _valKeluarga = value;
                      });
                    },
                  ),
                ],
              ),

              SizedBox(height: ScreenUtil.getInstance().setSp(50)),
              Row(
                children: < Widget > [
                  Icon(Icons.ac_unit, color: Colors.grey, ),
                  Padding(padding: EdgeInsets.only(right: 15)),
                  DropdownButton(
                    hint: Text("Pilih bidan"),
                    value: _valBidan,
                    items: dataBidan ?.map((item) {
                      return DropdownMenuItem(
                        child: Row(
                          children: < Widget > [
                            SizedBox(
                              width: 250,
                              child: Text(" " + item['bidan_nama'],
                                textAlign: TextAlign.left,
                                style: new TextStyle(color: Colors.black54),
                              ),

                            ),
                          ],
                        ),
                        value: item['bidan_id'],
                      );
                    }) ?.toList() ?? [],
                    onChanged : (value) {
                      setState(() {
                        _valBidan = value;
                      });
                    },
                  ),
                ],
              ),
            ],
          ),
        ),
      )
    );
  }

  _displayNotifikasi(BuildContext context, _scaffoldKey, String pesan) {
    final snackBar = SnackBar(content: Text(pesan));
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }

  Future < void > ambilDataKeluarga() async {
    final response =
      await http.post("${baseurl}keluarga/getKeluargaDropdown", headers: {
          "Accept": "application/json"
        },
        body: {
          "user_id": userId.toString(),
        }
      );
    setState(() {
      var convertDataToJson = json.decode(response.body);
      dataKeluarga = convertDataToJson['data']['keluarga'];
    });
  }

  Future < void > ambilDataBidan() async {
    final response =
      await http.get("${baseurl}bidan/getBidan", headers: {
        "Accept": "application/json"
      });
    setState(() {
      var convertDataToJson = json.decode(response.body);
      dataBidan = convertDataToJson['data']['bidan'];
    });
  }

  Future < void > getBeforeUpdateReservasi() async {
    final response =
      await http.post("${baseurl}reservasi/getBeforeUpdateReservasi", body: {
        "reservasi_id": reservasiId.toString(),
      });
    setState(() {
      var convertDataToJson = json.decode(response.body);
      dataReservasi = convertDataToJson['data']['reservasi'];
      _valKeluarga = (dataReservasi[0]["keluarga_id"] != null) ? dataReservasi[0]["keluarga_id"] : 0;
      _valBidan = (dataReservasi[0]["bidan_id"] != null) ? dataReservasi[0]["bidan_id"] : 0;
    });
  }

  Future < void > updateFormDetailReservasi() async {
    if (_valKeluarga == 0) {
      _displayNotifikasi(context, scaffoldFormDetailReservasi, 'Anda harus memilih Nama Anak');
    } else {
      final response = await http.post("${baseurl}reservasi/updateReservasi", body: {
        "reservasi_id": reservasiId.toString(),
        "keluarga_id": _valKeluarga.toString(),
        "bidan_id": _valBidan.toString(),
        "user_id": userId.toString(),
      }, headers: {
        "Accept": "application/json"
      });
      setState(() {
        var convertDataToJson = json.decode(response.body);
        res = convertDataToJson['status'];
      });

      if (res == 200) {
        _displayNotifikasi(context, scaffoldFormDetailReservasi, 'Data berhasil disimpan.');
        kembaliKeKeranjang();
      } else if (res == 201) {
        _displayNotifikasi(context, scaffoldFormDetailReservasi, 'Tidak ada data yang disimpan');
      } else {
        _displayNotifikasi(context, scaffoldFormDetailReservasi, 'Terjadi kesalahan saat menghubungkan ke server');
      }
    }
  }

  @override
  // ignore: must_call_super
  void initState() {
    this.ambilDataBidan();
    this.ambilDataKeluarga();
    this.getBeforeUpdateReservasi();
  }

  kembaliKeKeranjang() {
    Navigator.pop(context, true);
    var route = new MaterialPageRoute(
      builder: (BuildContext context) => new Keranjang(
        userId: userId,
      ),
    );
    Navigator.of(context).push(route);
  }
}