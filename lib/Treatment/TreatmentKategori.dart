import 'package:flutter/foundation.dart';
import 'package:kinara/Keluarga/Keluarga.dart';
import 'package:kinara/Keluarga/KeluargaHeader.dart';
import 'package:kinara/Profil/ProfilUserHeader.dart';
import 'package:kinara/Treatment/Keranjang.dart';
import 'package:kinara/Treatment/Treatment.dart';
import 'package:kinara/host/Url.dart';
import 'package:kinara/Profil/ProfilUser.dart';
import 'dart:convert';
import 'dart:async';
import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:http/http.dart'
as http;

// ignore: must_be_immutable
class TreatmentKategori extends StatefulWidget {
  TreatmentKategori({
    Key key,
    this.userId,
    this.userNama,
    this.userAlamat,
    this.userPassword,
    this.userEmail,
    this.userNotlp,
    this.updatedAt,
    this.createdAt,
    this.kembali,
  }): super(key: key);
  var userId, userNama, userAlamat, userPassword, userEmail, userNotlp, createdAt, updatedAt, kembali;

  @override
  _HomeState createState() => _HomeState(
    this.userId,
    this.userNama,
    this.userAlamat,
    this.userPassword,
    this.userEmail,
    this.userNotlp,
    this.updatedAt,
    this.createdAt,
    this.kembali,
  );
}

class _HomeState extends State < TreatmentKategori > {
  _HomeState(
    this.userId,
    this.userNama,
    this.userAlamat,
    this.userPassword,
    this.userEmail,
    this.userNotlp,
    this.updatedAt,
    this.createdAt,
    this.kembali
  );
  var userId, userNama, userAlamat, userPassword, userEmail, userNotlp, createdAt, updatedAt, kembali;
  List dataMenu = [];
  List dataKategori = [];
  List dataSlideShow = [];
  List dataKeluarga = [];
  int indexBottomNavigationBar = 0;

  @override
  Widget build(BuildContext context) {
    var lebarLayar = MediaQuery.of(context).size.width;
    var tinggiLayar = MediaQuery.of(context).size.height;

    
      setState(() {
        if (kembali != null) {
          bottomNavigationBarItemTab(kembali);
          kembali = null;
        }
      });
    

    return WillPopScope(
      // child: MaterialApp(
      //   routes: {
      //     "/": (context) => homeTreatmentKategori(lebarLayar, tinggiLayar, dataSlideShow, dataKategori),
      //   },
      // ),
      child: Scaffold(
        body: new ListView(
          children: < Widget > [
            Container(
              margin: const EdgeInsets.all(0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: tampilkanSelection(context, lebarLayar, tinggiLayar, indexBottomNavigationBar),
                ),
            ),
          ]
        ),
        bottomNavigationBar: BottomNavigationBar(
          currentIndex: indexBottomNavigationBar,
          type: BottomNavigationBarType.fixed,
          backgroundColor: Colors.blue[500],
          items: [
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              title: new Text('Home', )
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.shopping_cart),
              title: new Text('Keranjang', )
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.people),
              title: new Text('Keluarga', )
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.person),
              title: new Text('Saya', )
            ),
          ],
          selectedItemColor: Colors.white,
          onTap: (index) {
            bottomNavigationBarItemTab(index);
          },
        ),
      ),
      onWillPop: () {
        return showDialog(
          context: context,
          builder: (context) => new AlertDialog(
            title: Text("Kamu akan keluar dari Aplikasi ?"),
            actions: [
              FlatButton(
                onPressed: () => Navigator.of(context).pop(false),
                child: Text("Tidak"),
              ),
              FlatButton(
                onPressed: () => Navigator.of(context).pop(true),
                child: Text("Iya"),
              )
            ],
          )
        ) ?? false;

      }
    );
  }

  void bottomNavigationBarItemTab(index) {
    setState(() {
      indexBottomNavigationBar = index;
    });
  }

  Future < void > ambilDataKategori() async {
    http.Response hasilAmbilDataKategori = await http.get(
      Uri.encodeFull("${baseurl}treatmentKategori"), headers: {
        "Accept": "application/json"
      }
    );
    this.setState(() {
      Map < String, dynamic > dataKategoriDecode = json.decode(hasilAmbilDataKategori.body);
      dataKategori = dataKategoriDecode['data']['treatment_kategori'];
    });
  }

  Future < void > ambilDataSlideShow() async {
    http.Response hasilAmbilDataSlideShow = await http.get(
      Uri.encodeFull("${baseurl}slideshow"), headers: {
        "Accept": "application/json"
      }
    );
    this.setState(() {
      Map < String, dynamic > dataSlideShowDecode = json.decode(hasilAmbilDataSlideShow.body);
      dataSlideShow = dataSlideShowDecode['data']['slideshow'];
    });
  }

  Future < void > ambilDataMenu() async {
    http.Response hasilAmbilDataMenu = await http.get(
      Uri.encodeFull("${baseurl}menu/getMenu"), headers: {
        "Accept": "application/json"
      }
    );
    this.setState(() {
      Map < String, dynamic > dataMenuDecode = json.decode(hasilAmbilDataMenu.body);
      dataMenu = dataMenuDecode['data']['menu'];
    });
  }

  Future ambilDataKeluarga() async {
    http.Response hasilAmbilDataKeluarga = await http.post(
      Uri.encodeFull("${baseurl}keluarga/getKeluarga"), headers: {
        "Accept": "application/json"
      },
      body: {
        "user_id": userId.toString(),
      }
    );
    this.setState(() {
      Map < String, dynamic > dataKeluargaDecode = json.decode(hasilAmbilDataKeluarga.body);
      dataKeluarga = dataKeluargaDecode['data']['keluarga'];
    });
  }

  @override
  // ignore: must_call_super
  void initState() {
    this.ambilDataKategori();
    this.ambilDataSlideShow();
    this.ambilDataMenu();
    this.ambilDataKeluarga();
  }

  homeTreatmentKategori(lebarLayar, tinggiLayar, dataSlideShow, dataKategori) {
    return Scaffold(
      body: new ListView(
        children: < Widget > [
          Container(
            margin: const EdgeInsets.all(0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: tampilkanSelection(context, lebarLayar, tinggiLayar, indexBottomNavigationBar),
              ),
          ),
        ]
      ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: indexBottomNavigationBar,
        type: BottomNavigationBarType.fixed,
        backgroundColor: Colors.blue[500],
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            title: new Text('Home', )
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.shopping_cart),
            title: new Text('Keranjang', )
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.people),
            title: new Text('Keluarga', )
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person),
            title: new Text('Saya', )
          ),
        ],
        selectedItemColor: Colors.white,
        onTap: (index) {
          bottomNavigationBarItemTab(index);
        },
      ),
    );
  }

  profilPerusahaan() {
    return Container(
      padding: EdgeInsets.all(10),
      decoration: BoxDecoration(
        border: Border.all(width: 0.4, color: Colors.grey),
        borderRadius: BorderRadius.circular(0)
      ),
      child: Row(
        children: < Widget > [
          Container(
            width: 25,
            height: 50,
            margin: EdgeInsets.only(right: 10),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(25),
              child: Image.network(
                'https://kinarababycare.com/wp-content/uploads/2020/03/logo.png',
                fit: BoxFit.cover,
              ),
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: < Widget > [
              Text(
                'KINARA',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20, letterSpacing: 2, color: Colors.blue[700]),
              ),
              Text('Mom & Baby Care',
                style: TextStyle(fontSize: 14, letterSpacing: 1, color: Colors.blue[700]),
              )
            ],
          ),
          Spacer(
            flex: 2,
          ),
        ],
      )
    );
  }

  kategoriTreatmentJudul() {
    return Container(
      alignment: Alignment.center,
      child: Padding(
        padding: EdgeInsets.only(top: 70, bottom: 25),
        child: Text('PILIH KATEGORI TREATMENT',
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15, letterSpacing: 3, color: Colors.black54),
        ),
      ),
    );
  }

  kategoriTreatment(dataKategori) {
    return Container(
      height: 110,
      child: Container(
        child: ListView.separated(
          physics: PageScrollPhysics(),
          separatorBuilder: (context, index) => Divider(
            indent: 3,
          ),
          scrollDirection: Axis.horizontal,
          itemCount: dataKategori == null ? 0 : dataKategori.length,
          itemBuilder: (context, index) => boxKategori(context, index),
        ),
      ),
    );
  }

  boxKategori(context, index) {
    return GestureDetector(
      onTap: () {
        var route = new MaterialPageRoute(
          builder: (BuildContext context) => new Treatment(
            userId: userId,
            userNama: userNama,
            userPassword: userPassword,
            userEmail: userEmail,
            userNotlp: userNotlp,
            updatedAt: updatedAt,
            createdAt: createdAt,
            treatmentKategoriId: dataKategori[index]['treatment_kategori_id'],
            treatmentKategoriNama: dataKategori[index]['treatment_kategori_nama'],
          ),
        );
        Navigator.of(context).push(route);
      },
      child: SizedBox(
        width: (MediaQuery.of(context).size.width / 3) - 3,
        child: Container(
          alignment: Alignment.center,
          margin: EdgeInsets.only(right: 0, left: 3),
          padding: EdgeInsets.all(1),
          decoration: BoxDecoration(
            border: Border.all(width: 0.3, color: Colors.grey),
            borderRadius: BorderRadius.circular(10)),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: < Widget > [
              Container(
                height: 70,
                width: 70,
                child: Image(
                  fit: BoxFit.contain,
                  alignment: Alignment.center,
                  image: NetworkImage(
                    dataKategori[index]['treatment_kategori_icon'],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 5),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: < Widget > [
                      Text(
                        dataKategori[index]['treatment_kategori_nama'],
                        style: TextStyle(fontWeight: FontWeight.bold),
                        maxLines: 2,
                      ),
                    ],
                  ),
              )
            ],
          ),
        ),
      ),
    );
  }

  imageSliderJudul() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: < Widget > [
        Padding(
          padding: EdgeInsets.only(top: 40, bottom: 20),
          child: Text('Layanan di Kinara Baby Care',
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15, letterSpacing: 1, color: Colors.black54),
          ),
        ),
      ],
    );
  }

  imageSlider(dataSlideShow) {
    if (dataSlideShow.length != 0) {
      return CarouselSlider.builder(
        autoPlay: true,
        height: 150.0,
        itemCount: dataSlideShow == null ? 0 : dataSlideShow.length,
        itemBuilder: (context, i) {
          return Container(
            width: MediaQuery.of(context).size.width,
            margin: EdgeInsets.symmetric(horizontal: 0.0),
            decoration: BoxDecoration(
              color: Colors.blue[50]
            ),
            child: Image(
              fit: BoxFit.cover,
              alignment: Alignment.center,
              image: NetworkImage(dataSlideShow[i]['slideshow_icon']),
            ),
          );
        });
    } else {
      return Container(
        alignment: AlignmentDirectional.center,
        child: Text('Sedang mengambil data .. .'),
      );
    }
  }
  
  tampilkanSelection(context, lebarLayar, tinggiLayar, int indexBottomNavigationBar) {
    switch (indexBottomNavigationBar) {
      case 0:
        return <Widget > [
          profilPerusahaan(),
          // imageSliderJudul(),
          imageSlider(dataSlideShow),
          kategoriTreatmentJudul(),
          kategoriTreatment(dataKategori),
        ];
        break;
      case 1:
        WidgetsBinding.instance.addPostFrameCallback((_) {
          var route = new MaterialPageRoute(
            builder: (BuildContext context) => Keranjang(
              userId: userId,
              kembali: 0,
            ),
          );
          Navigator.of(context).push(route);
        });
      break;
      case 2:
        return <Widget > [
          keluargaHeader(context, userId, userNama, userEmail, userAlamat, userNotlp, dataKeluarga),
          keluarga(lebarLayar, tinggiLayar, userId, dataKeluarga),
        ];
        break;
      case 3:
        return <Widget > [
          profilUserHeader(context, userId, userNama, userEmail, userAlamat, userNotlp, dataKeluarga),
          profilUser(lebarLayar, tinggiLayar, userId),
        ];
        break;
      default:
        profilPerusahaan();
    }
  }

}