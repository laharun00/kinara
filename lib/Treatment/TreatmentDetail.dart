import 'package:flutter/material.dart';
import 'package:kinara/Treatment/Keranjang.dart';
import 'package:kinara/host/Url.dart';
import 'dart:convert';
import 'dart:async';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart';
import 'package:http/http.dart'
as http;

// ignore: must_be_immutable, camel_case_types
class Treatment_detail extends StatefulWidget {
  Treatment_detail({
    this.userId,
    this.treatmentId,
    this.treatmentNama,
  });
  int userId;
  int treatmentId;
  String treatmentNama;

  _Treatment_detailState createState() =>
    _Treatment_detailState(this.userId, this.treatmentNama, this.treatmentId);
}

// ignore: camel_case_types
class _Treatment_detailState extends State < Treatment_detail > {
  _Treatment_detailState(this.userId, this.treatmentNama, this.treatmentId);
  final formatter = new NumberFormat("#,###");
  final _scaffoldKey = GlobalKey < ScaffoldState > ();
  var jumlamReservasi = 0;
  List treatmentFoto = [];
  int userId;
  int treatmentId;
  int treatmentHarga;
  String treatmentNama;
  String treatmentKeterangan;

  // ignore: missing_return
  Future < String > ambilData() async {
    http.Response hasil = await http.get(
      Uri.encodeFull("${baseurl}treatmentDetail/$treatmentId"),
      headers: {
        "Accept": "application/json"
      });

    this.setState(() {
      Map < String, dynamic > data = json.decode(hasil.body);
      treatmentKeterangan = data['data']['treatment']['treatment_keterangan'];
      treatmentHarga = data['data']['treatment']['treatment_harga'];
      treatmentFoto = data['data']['treatment_foto'];
    });
  }

  @override
  // ignore: must_call_super
  void initState() {
    this.ambilData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        actions: < Widget > [
          Stack(
            children: < Widget > [
              IconButton(
                padding: EdgeInsets.only(right: 30.0, left: 0.0, top: 10.0),
                icon: Icon(Icons.shopping_cart),
                color: Colors.blue[500],
                onPressed: () {
                  var route = new MaterialPageRoute(
                    builder: (BuildContext context) => Keranjang(
                      userId: userId,
                    ),
                  );
                  Navigator.of(context).push(route);
                },
              ),
              new Positioned(
                top: 8.0,
                right: 0.0,
                left: 0.0,
                child: new Center(
                  child: new Text(
                    '$jumlamReservasi',
                    style: new TextStyle(
                      color: Colors.blue[700],
                      fontSize: 15.0,
                      fontWeight: FontWeight.w500),
                  ),
                )),
            ],
          )
        ],
        backgroundColor: Colors.white,
        leading: IconButton(
          color: Colors.blue[700],
          icon: Icon(Icons.arrow_back),
          onPressed: () => Navigator.pop(context, true)),
      ),
      body: detailBody(),
    );
  }

  detailBody() {
    jumlahReservasi(userId.toString());
    return ListView(
      children: < Widget > [
        new Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.max,
          children: < Widget > [
            new Container(
              padding: EdgeInsets.only(
                left: 0.0, right: 0.0, top: 0.0, bottom: 0.0),
              child: new ListView.builder(
                // scrollDirection: Axis.vertical,
                shrinkWrap: true,
                itemCount: treatmentFoto == null ? 0 : treatmentFoto.length,
                itemBuilder: (context, i) {
                  return Container(
                    padding: EdgeInsets.only(
                      left: 0.0, right: 0.0, top: 0.0, bottom: 0.0),
                    child: ClipRRect(
                      borderRadius: new BorderRadius.circular(0.0),
                      child: Image(
                        fit: BoxFit.contain,
                        alignment: Alignment.center,
                        image: NetworkImage(
                          treatmentFoto[i]['treatment_foto_file']),
                      ),
                    ),
                  );
                })),
            new Padding(
              padding: EdgeInsets.only(left: 16.0, right: 16.0, top: 30.0),
              child: Container(
                alignment: Alignment.center,
                child:
                Column(
                  children: < Widget > [
                    Text(
                      "$treatmentNama".toUpperCase(),
                      style: TextStyle(
                        fontSize: ScreenUtil.getInstance().setSp(40),
                        fontFamily: "Poppins-Bold",
                        letterSpacing: .20,
                        wordSpacing: .50,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    Container(
                      alignment: Alignment.center,
                      padding: EdgeInsets.only(
                        left: 10.0, right: 10.0, top: 10.0, bottom: 0),
                      child: Container(
                        width: 170.0,
                        height: 40.0,
                        alignment: Alignment.bottomRight,
                        child: FloatingActionButton.extended(
                          backgroundColor: Colors.pink,
                          onPressed: () {
                            reservasi(userId.toString(), treatmentId.toString());
                          },
                          elevation: 0.0,
                          label: Text('Pilih Treatment'),
                          icon: Icon(Icons.add_shopping_cart),
                        ),
                      )
                    ),
                  ],
                ),


              ),
            ),
            new Padding(
              padding: EdgeInsets.only(left: 20.0, right: 20.0, top: 30.0),
              child: Container(
                alignment: Alignment.center,
                child: Text(
                  treatmentKeterangan == null ? '' : treatmentKeterangan,
                  style: TextStyle(
                    color: Colors.black45,
                    fontSize: ScreenUtil.getInstance().setSp(30),
                    fontFamily: "Poppins-Bold",
                    fontWeight: FontWeight.w500,
                    letterSpacing: .20,
                    wordSpacing: .50,
                  ),
                  textAlign: TextAlign.justify,
                ),
              ),
            ),
            new Padding(
              padding: EdgeInsets.only(left: 20.0, right: 20.0, top: 20.0, bottom: 20.0),
              child: Container(
                child: Text(
                  'Harga : Rp ' +
                  formatter
                  .format(treatmentHarga == null ? 0 : treatmentHarga),
                  style: TextStyle(
                    color: Colors.black45,
                    fontSize: ScreenUtil.getInstance().setSp(30),
                    fontFamily: "Poppins-Bold",
                    fontWeight: FontWeight.bold,
                    letterSpacing: .20,
                    wordSpacing: .50,
                  ),
                  textAlign: TextAlign.left,
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }

  Future < void > reservasi(String userId, String treatmentId) async {
    final response =
      await http.post("${baseurl}reservasi/add", body: {
        "user_id": userId,
        "treatment_id": treatmentId,
        "alamat": 'yogya',
      });
    setState(() {
      var convertDataToJson = json.decode(response.body);
      if (convertDataToJson['status'] == 200) {
        _displayNotifikasi(context, _scaffoldKey, 'Treatment dimasukan ke keranjang');
      } else {
        _displayNotifikasi(context, _scaffoldKey, 'Terjadi kesalahan');
      }
    });
    jumlahReservasi(userId);
  }

  Future < void > jumlahReservasi(String userId) async {
    var jml = 0;
    final response =
      await http.post("${baseurl}reservasi/jumlahReservasi", body: {
        "user_id": userId,
      });
    if (this.mounted) {
      setState(() {
        var convertDataToJson = json.decode(response.body);
        if (convertDataToJson['status'] == 200) {
          jml = convertDataToJson['data']['jumlahReservasi'];
        } else {
          jml = 0;
        }
        jumlamReservasi = jml;
      });
    }
  }
}

_displayNotifikasi(BuildContext context, _scaffoldKey, String pesan) {
  final snackBar = SnackBar(content: Text(pesan));
  _scaffoldKey.currentState.showSnackBar(snackBar);
}