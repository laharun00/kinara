import 'dart:convert';
import 'dart:async';
import 'package:intl/intl.dart';
import 'package:kinara/Treatment/Keranjang.dart';
import 'package:kinara/host/Url.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart'
as http;

// ignore: must_be_immutable
class Treatment extends StatefulWidget {
  Treatment({
    Key key,
    this.userId,
    this.userNama,
    this.userPassword,
    this.userEmail,
    this.userNotlp,
    this.updatedAt,
    this.createdAt,
    this.treatmentKategoriId,
    this.treatmentKategoriNama
  }): super(key: key);
  var userId, userNama, userPassword, userEmail, userNotlp, createdAt, updatedAt, treatmentKategoriId, treatmentKategoriNama;

  _TreatmentState createState() => _TreatmentState(this.userId, this.treatmentKategoriId, this.treatmentKategoriNama);
}

class _TreatmentState extends State < Treatment > {
  _TreatmentState(this.userId, this.treatmentKategoriId, this.treatmentKategoriNama);
  final formatter = new NumberFormat("#,###");
  final _scaffoldKey = GlobalKey < ScaffoldState > ();
  List dataTreatment;
  var treatmentKategoriId;
  var treatmentKategoriNama;
  var userId;
  var jumlamReservasi = 0;
  int treatmentHarga;
  String treatmentNama;
  String treatmentKeterangan;
  List treatmentFoto = [];

  Future < void > ambilData() async {
    http.Response hasil = await http.get(
      Uri.encodeFull("${baseurl}treatmentById/$treatmentKategoriId"), headers: {
        "Accept": "application/json"
      }
    );

    this.setState(() {
      Map < String, dynamic > data = json.decode(hasil.body);
      dataTreatment = data["data"]["treatment"];
    });
  }

  @override
  // ignore: must_call_super
  void initState() {
    this.ambilData();
  }

  @override
  Widget build(BuildContext context) {
    jumlahReservasi(userId.toString());
    return Scaffold(
      key: _scaffoldKey,
      appBar: _buildBar(context),
      body: new ListView.builder(
        itemCount: dataTreatment == null ? 0 : dataTreatment.length,
        itemBuilder: (context, i) {
          return GestureDetector(
            onTap: () => treatmentDetail(this.userId, dataTreatment[i]['treatment_id'], dataTreatment[i]['treatment_nama']),
            child: SizedBox(
              height: 124.0,
              child: Card(
                shadowColor: Colors.white,
                elevation: 0.2,
                margin: EdgeInsets.all(1),
                child: Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 10),
                        child: Image(
                          height: 100,
                          width: 100,
                          fit: BoxFit.cover,
                          alignment: Alignment.center,
                          image: NetworkImage(dataTreatment[i]['treatment_foto_file']),
                        ),
                    ),
                    Column(
                      children: [
                        Container(
                          width: MediaQuery.of(context).size.width * 0.63,
                          padding: const EdgeInsets.only(left: 20, top: 14),
                            child: Text(dataTreatment[i]['treatment_nama'].toUpperCase(),
                              style: TextStyle(fontSize: 18, letterSpacing: 1, fontWeight: FontWeight.bold)),
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width * 0.63,
                          padding: const EdgeInsets.only(left: 20, top: 8),
                            child: Text(
                              (dataTreatment[i]['treatment_narasi'] != null) ? dataTreatment[i]['treatment_narasi'] : '',
                              style: TextStyle(height: 1.5, fontSize: 13, fontWeight: FontWeight.w400, letterSpacing: 0.2, ),
                            ),
                        ),
                        Row(
                          children: [
                            Container(
                              width: MediaQuery.of(context).size.width * 0.46,
                              alignment: Alignment.centerLeft,
                              padding: const EdgeInsets.only(left: 20),
                                child:
                                Row(
                                  children: [
                                    Text("Selengkapnya", style: TextStyle(fontSize: 14, fontWeight: FontWeight.w400, color: Colors.green)),
                                    Icon(Icons.arrow_drop_down, color: Colors.green, ),
                                  ],
                                ),

                            ),
                            Container(
                              alignment: Alignment.centerRight,
                              child: ButtonTheme(
                                minWidth: 0,
                                height: 20,
                                child: RaisedButton(
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(18.0)
                                  ),
                                  color: Colors.green,
                                  elevation: 0,
                                  onPressed: () {
                                    reservasi(userId.toString(), dataTreatment[i]['treatment_id'].toString());
                                  },
                                  child: Text("Pilih",
                                    style: TextStyle(fontSize: 13, fontWeight: FontWeight.w500, color: Colors.white))
                                ),
                              )
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          );
        }
      ),
    );
  }

  Widget _buildBar(BuildContext context) {
    return AppBar(
      elevation: 0.3,
      centerTitle: true,
      title: Text('$treatmentKategoriNama', style: TextStyle(color: Colors.black)),
      backgroundColor: Colors.white,
      leading: IconButton(
        color: Colors.blue[700],
        icon: Icon(Icons.arrow_back),
        onPressed: () => Navigator.pop(context, true)),
      actions: < Widget > [
        Stack(
          children: < Widget > [
            IconButton(
              padding: EdgeInsets.only(right: 30.0, left: 0.0, top: 10.0),
              icon: Icon(Icons.shopping_cart),
              color: Colors.blue[500],
              onPressed: () {
                var route = new MaterialPageRoute(
                  builder: (BuildContext context) => Keranjang(
                    userId: userId,
                  ),
                );
                Navigator.of(context).push(route);
              },
            ),
            new Positioned(
              top: 8.0,
              right: 0.0,
              left: 0.0,
              child: new Center(
                child: new Text(
                  '$jumlamReservasi',
                  style: new TextStyle(
                    color: Colors.blue[700],
                    fontSize: 15.0,
                    fontWeight: FontWeight.w500),
                ),
              )),
          ],
        )
      ],
    );
  }

  treatmentDetail(int userId, int treatmentId, String treatmentNama) async {
    // var route = new MaterialPageRoute(
    //   builder: (BuildContext context) => new Treatment_detail(
    //     userId: userId,
    //     treatmentId: treatmentId,
    //     treatmentNama: treatmentNama,
    //   ),
    // );
    // Navigator.of(context).push(route);

    http.Response hasil = await http.get(
      Uri.encodeFull("${baseurl}treatmentDetail/$treatmentId"),
      headers: {
        "Accept": "application/json"
      });

    this.setState(() {
      Map < String, dynamic > data = json.decode(hasil.body);
      treatmentKeterangan = data['data']['treatment']['treatment_keterangan'];
      treatmentHarga = data['data']['treatment']['treatment_harga'];
      treatmentFoto = data['data']['treatment_foto'];
    });

    showModalBottomSheet(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(18.0)
      ),
      isScrollControlled: true,
      context: context,
      // enableDrag: true,
      builder: (context) {
        return SingleChildScrollView(
          child: Container(
            height: MediaQuery.of(context).size.height * .96,
            child: Padding(
              padding: const EdgeInsets.all(20.0),
                child: Column(
                  children: < Widget > [
                    Container(
                      child: Text(
                        "$treatmentNama".toUpperCase(),
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontFamily: "Poppins-Bold",
                          letterSpacing: .20,
                          wordSpacing: .50,
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(top: 10.0),
                      child: Image(
                        fit: BoxFit.contain,
                        alignment: Alignment.center,
                        image: NetworkImage(
                          treatmentFoto[0]['treatment_foto_file']),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(top: 10.0),
                      child: Text(
                        treatmentKeterangan == null ? '' : treatmentKeterangan,
                        style: TextStyle(height: 1.5),
                        textAlign: TextAlign.justify,
                      ),
                    ),
                    Container(
                      alignment: Alignment.center,
                      padding: EdgeInsets.only(top: 10.0),
                      child: Text('Harga : Rp ' + formatter.format(treatmentHarga == null ? 0 : treatmentHarga),
                        style: TextStyle(height: 1.5),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(top: 10.0),
                      alignment: Alignment.center,
                      child: Column(
                        children: < Widget > [
                          Container(
                            alignment: Alignment.center,
                            padding: EdgeInsets.only(
                              left: 10.0, right: 10.0, top: 10.0, bottom: 0),
                            child: Container(
                              width: 170.0,
                              height: 40.0,
                              alignment: Alignment.bottomRight,
                              child: FloatingActionButton.extended(
                                backgroundColor: Colors.pink,
                                onPressed: () {
                                  reservasi(userId.toString(), treatmentId.toString());
                                  Navigator.pop(context);
                                },
                                elevation: 0.0,
                                label: Text('Pilih Treatment'),
                                icon: Icon(Icons.add_shopping_cart),
                              ),
                            )
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
            ),
          ),
        );
      }
    );
  }

  Future < void > reservasi(String userId, String treatmentId) async {
    final response =
      await http.post("${baseurl}reservasi/add", body: {
        "user_id": userId,
        "treatment_id": treatmentId,
        "alamat": 'yogya',
      });
    setState(() {
      var convertDataToJson = json.decode(response.body);
      if (convertDataToJson['status'] == 200) {
        _displayNotifikasi(context, _scaffoldKey, 'Treatment dimasukan ke keranjang');
      } else {
        _displayNotifikasi(context, _scaffoldKey, 'Terjadi kesalahan');
      }
    });
    jumlahReservasi(userId);
  }

  Future < void > jumlahReservasi(String userId) async {
    var jml = 0;
    final response =
      await http.post("${baseurl}reservasi/jumlahReservasi", body: {
        "user_id": userId,
      });
    if (this.mounted) {
      setState(() {
        var convertDataToJson = json.decode(response.body);
        if (convertDataToJson['status'] == 200) {
          jml = convertDataToJson['data']['jumlahReservasi'];
        } else {
          jml = 0;
        }
        jumlamReservasi = jml;
      });
    }
  }

}

class FiledTreatment {
  final int treatmentId;
  final String treatmentNama;
  final String treatmentKeterangan;
  final int treatmentHarga;
  FiledTreatment(this.treatmentId, this.treatmentNama, this.treatmentKeterangan, this.treatmentHarga);
}

_displayNotifikasi(BuildContext context, _scaffoldKey, String pesan) {
  final snackBar = SnackBar(content: Text(pesan));
  _scaffoldKey.currentState.showSnackBar(snackBar);
}