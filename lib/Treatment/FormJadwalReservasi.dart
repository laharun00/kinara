import 'dart:async';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:geocoder/geocoder.dart';
import 'package:geolocator/geolocator.dart';
import 'package:kinara/Treatment/Keranjang.dart';
// import 'package:path/path.dart';

// ignore: must_be_immutable
class FormJadwalReservasi extends StatefulWidget {
  FormJadwalReservasi({
    this.userId
  });
  int userId;

  @override
  _FormJadwalReservasiState createState() => _FormJadwalReservasiState(this.userId);
}

class _FormJadwalReservasiState extends State < FormJadwalReservasi > {
  // final _keyFormKadwalReservasi = GlobalKey < ScaffoldState > ();
  int userId;

  _FormJadwalReservasiState(
    this.userId  
  );

  Position koordinatSaiki;
  Set < Marker > markers;
  double setAwalLatiCameraPosition;
  double setAwalLongCameraPosition;

  Future ambilKoor() async {
    // Geolocator geolocator = Geolocator()..forceAndroidLocationManager = true;
    // GeolocationStatus geolocationStatus = await geolocator.checkGeolocationPermissionStatus();
    Position position = await Geolocator().getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
    // var locationOptions = LocationOptions(accuracy: LocationAccuracy.high, distanceFilter: 10);
    // StreamSubscription < Position > positionStream = geolocator.getPositionStream(locationOptions).listen(
    //   (Position position) {
    //     print(position == null ? 'Unknown' : position.latitude.toString() + ', ' + position.longitude.toString());
    //   });
    // setState(() {
      koordinatSaiki = position;
    // });
    // return null;
  }

  @override
  // ignore: must_call_super
  void initState() {
    this.ambilKoor();
    markers = Set.from([]);
    // markers.add(
    //   Marker(
    //     markerId: MarkerId("1"),
    //     position: LatLng(-7.8016812,110.3633106),
    //     icon: BitmapDescriptor.defaultMarker,
    //   ),
    // );
    // getInfoPlace(context,-7.8016812,110.3633106);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    ambilKoor();
    if (koordinatSaiki == null) {
      setAwalLatiCameraPosition = -7.8016812;
      setAwalLongCameraPosition = 110.3633106;
    } else {
      setAwalLatiCameraPosition = koordinatSaiki.latitude;
      setAwalLongCameraPosition = koordinatSaiki.longitude;
    }
    // print(koordinatSaiki);

    return Scaffold(
      body: GoogleMap(
        markers: markers,
        onMapCreated: (GoogleMapController controller) {},
        mapType: MapType.normal,
        initialCameraPosition: CameraPosition(
          target: LatLng(setAwalLatiCameraPosition, setAwalLongCameraPosition),
          zoom: 14.4746,
        ),
        onTap: (pos) {
          // Marker m = Marker(markerId: MarkerId("1"), position: pos, icon: BitmapDescriptor.defaultMarker);
          // markers.add(m);
          setState(() {
            markers.add(Marker(markerId: MarkerId("1"), position: pos, icon: BitmapDescriptor.defaultMarker));
          });
          getInfoPlace(context, pos.latitude, pos.longitude);
        },
      ),
    );
  }

  Future < Function > getInfoPlace(context, latitude, longitude) async {
    // final query = "1600 Amphiteatre Parkway, Mountain View";
    // var addresses = await Geocoder.local.findAddressesFromQuery(query);
    // var first = addresses.first;
    // print("${first.featureName} : ${first.coordinates}");

    final coordinates = new Coordinates(latitude, longitude);
    var addresses = await Geocoder.local.findAddressesFromCoordinates(coordinates);
    var first = addresses.first;
    // print("${first.featureName} : ${first.addressLine}");
    showModalBottomSheet(
      context: context,
      // isDismissible: false,
      enableDrag: true,
      builder: (BuildContext bc) {
        return Container(
          height: MediaQuery.of(context).size.height * .25,
          child: Padding(
            padding: const EdgeInsets.all(10.0),
              child: Column(
                children: < Widget > [
                  Row(
                    children: < Widget > [
                      Container(
                        padding: EdgeInsets.only(bottom: 10),
                        child: Text('Detial alamat', style: TextStyle(fontSize: 20)),
                      ),
                      // Spacer(),
                      // IconButton(
                      //   icon: Icon(Icons.cancel, color: Colors.red),
                      //   onPressed: () {
                      //     Navigator.of(context).pop();
                      //   }),
                    ],
                  ),
                  Row(
                    children: < Widget > [
                      Container(
                        padding: EdgeInsets.only(bottom: 10),
                        width: MediaQuery.of(context).size.width - 60,
                        child: Text(first.addressLine, overflow: TextOverflow.ellipsis, maxLines: 3),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Container(
                        padding: EdgeInsets.only(right: 10),
                        child: RaisedButton(
                          onPressed: () {
                            setState(() {
                              markers.clear();
                            });
                            Navigator.of(context).pop();
                          },
                          textColor: Colors.white,
                          color: Colors.red,
                          child: Text('Batal'),
                          padding: const EdgeInsets.all(0.0),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(right: 20),
                        child: RaisedButton(
                          onPressed: () {
                            var route = MaterialPageRoute(builder: (context)=>Keranjang(
                              userId: userId,
                              latitude : latitude,
                              longitude: longitude,
                              tempatLatiLong: first.addressLine
                              ));
                              Navigator.of(context).push(route);
                          },
                          textColor: Colors.white,
                          color: Colors.blue,
                          child: Text('Set lokasi'),
                          padding: const EdgeInsets.all(0.0),
                        )
                      ),
                    ],
                  ),
                ],
              ),
          ),
        );
      });
    return null;
  }



}