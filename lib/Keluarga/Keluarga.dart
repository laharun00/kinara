import 'package:flutter/material.dart';
// import 'package:kinara/host/Api.dart';

keluarga(lebarLayar, tinggiLayar, userId, dataKeluarga) {
  return Container(
    width: lebarLayar,
    height: tinggiLayar,
    decoration: BoxDecoration(
      image: DecorationImage(
        fit: BoxFit.fitHeight,
        colorFilter: new ColorFilter.mode(Colors.black.withOpacity(0.2), BlendMode.dstATop),
        image: NetworkImage("https://3.bp.blogspot.com/-Kjvdq7atrkk/W-1a8fK11OI/AAAAAAAAAhk/BpTZWiLf6esJC60snzJbuXVIAzyBI49QgCHMYCw/s800/wallpaper-iphone-tumblr-hd-496-iphone-wallpaper.jpg"),
      ),
    ),
    alignment: AlignmentDirectional.center,
    child: Column(
      children: [
        Padding(
          padding: EdgeInsets.only(top: 40, bottom: 0, left: 60, right: 60),
          child: Text(
            'Anggota Keluarga',
            style: TextStyle(
              height: 1.5,
              fontSize: 15,
              fontStyle: FontStyle.normal,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        Padding(padding: EdgeInsets.only(top: 10, bottom: 0, left: 60, right: 60)),
        DataTable(
          columns: [
            DataColumn(label: Text('Nama', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold))),
            DataColumn(label: Text('Kelamin', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold))),
            DataColumn(label: Text('Lahir', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold))),
          ],
          rows: List.generate(
            dataKeluarga.length, (index) => _getDataRow(dataKeluarga[index])),
        ),
      ],
    ),
  );
}

DataRow _getDataRow(dataKeluarga) {
  return DataRow(
    cells: < DataCell > [
      DataCell(Text(dataKeluarga["keluarga_nama"])),
      DataCell(Text(dataKeluarga["keluarga_jenis_kelamin_singkatan"])),
      DataCell(Text(dataKeluarga["keluarga_tgl_lahir"])),
    ],
  );
}
 