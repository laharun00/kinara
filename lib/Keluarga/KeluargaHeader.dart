import 'package:flutter/material.dart';
import 'package:kinara/Keluarga/KeluargaUpdate.dart';

keluargaHeader(context, userId, userNama, userEmail, userAlamat, userNotlp, dataKeluarga) {
  return Container(
    padding: EdgeInsets.all(10),
    decoration: BoxDecoration(
      border: Border.all(width: 0.4, color: Colors.grey),
      borderRadius: BorderRadius.circular(0)
    ),
    child: Row(
      children: < Widget > [
        Container(
          width: 25,
          height: 50,
          margin: EdgeInsets.only(right: 10),
          child: ClipRRect(
            borderRadius: BorderRadius.circular(25),
            child: Image.network(
              'https://kinarababycare.com/wp-content/uploads/2020/03/logo.png',
              fit: BoxFit.cover, 
            ),
          ),
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: < Widget > [
            Text(
              'KINARA',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20, letterSpacing: 2, color: Colors.blue[700]),
            ),
            Text('Mom & Baby Care',
              style: TextStyle(fontSize: 14, letterSpacing: 1, color: Colors.blue[700]),
            )
          ],
        ),
        Spacer(
          flex: 2,
        ),
        IconButton(
          icon: Icon(Icons.mode_edit, color: Colors.blue, ),
          onPressed: () {
            Navigator.push(context, MaterialPageRoute(builder: (context) => KeluargaUpdate(
              userId: userId
            )));
          }
        ),
      ],
    )
  );
}