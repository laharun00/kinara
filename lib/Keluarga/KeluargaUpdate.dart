import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:kinara/Treatment/TreatmentKategori.dart';
import 'package:kinara/host/Url.dart';
import 'dart:convert';
import 'dart:async';
import 'package:http/http.dart'
as http;

// ignore: must_be_immutable
class KeluargaUpdate extends StatefulWidget {
  KeluargaUpdate({
    this.userId
  });
  var userId;

  @override
  _KeluargaUpdateState createState() => _KeluargaUpdateState(this.userId);
}

class _KeluargaUpdateState extends State < KeluargaUpdate > {
  _KeluargaUpdateState(this.userId);
  var userId;
  var keluargaNama = TextEditingController();
  var tanggalLahir = TextEditingController();
  int jenisKelamin;
  List dataKeluarga;
  final scaffoldKeluargaUpdate = GlobalKey < ScaffoldState > ();
  final _formTambahKeluarga = GlobalKey < FormState > ();
  final format = DateFormat("yyyy-MM-dd");

  @override
  Widget build(BuildContext context) {
    return
    WillPopScope(
      child: Scaffold(
        key: scaffoldKeluargaUpdate,
        appBar: AppBar(
          backgroundColor: Colors.white,
          leading: IconButton(
            color: Colors.blue[700],
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              _tombolKembaliDitekan();
            }
          ),
        ),
        body: ListView.separated(
          itemCount: dataKeluarga == null ? 0 : dataKeluarga.length,
          itemBuilder: (context, i) {
            return ListTile(
              leading: IconButton(icon: Icon(Icons.delete, color: Colors.red[400]), onPressed: () => _hapusKeluarga(dataKeluarga[i]["keluarga_id"])),
              title: Text(dataKeluarga[i]["keluarga_nama"], style: TextStyle(height: 1.8)),
              subtitle: Text("Jenis kelamin : " + dataKeluarga[i]["keluarga_jenis_kelamin"] + "\nTanggal lahir : " + dataKeluarga[i]["keluarga_tgl_lahir"], style: TextStyle(height: 1.8, )),
            );
          },
          separatorBuilder: (context, index) {
            return Divider();
          },
        ),
        floatingActionButton: FloatingActionButton.extended(
          onPressed: () {
            showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  content: Stack(
                    overflow: Overflow.visible,
                    children: < Widget > [
                      Form(
                        key: _formTambahKeluarga,
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: < Widget > [
                            Text("Tambah Keluarga"),
                            Padding(
                              padding: EdgeInsets.all(0),
                              child: TextFormField(
                                validator: (value) {
                                  if (value.isEmpty) {
                                    return "Nama tidak boleh kosong";
                                  } else {
                                    return null;
                                  }
                                },
                                textCapitalization: TextCapitalization.words,
                                decoration: InputDecoration(
                                  labelText: 'Nama',
                                  hintStyle: TextStyle(color: Colors.grey, fontSize: 16.0),
                                ),
                                controller: keluargaNama,
                              ),
                            ),
                            DateTimeField(
                              controller: tanggalLahir,
                              format: format,
                              onShowPicker: (context, currentValue) {
                                return showDatePicker(
                                  context: context,
                                  firstDate: DateTime(1900),
                                  initialDate: currentValue ?? DateTime.now(),
                                  lastDate : DateTime(2100),
                                );
                              },
                              decoration: InputDecoration(
                                labelText: "Tanggal lahir",
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(right: 8.0, left: 0.0, bottom: 8.0, top: 0.0),
                              child: DropdownButtonFormField < int > (
                                value: jenisKelamin,
                                items: [1, 2]
                                .map((label) => DropdownMenuItem(
                                  child: Text((label == 1) ? 'Laki-laki' : 'Perempuan'),
                                  value: label,
                                ))
                                .toList(),
                                hint: Text('Pilih Jenis kelamin'),
                                onChanged: (value) {
                                  setState(() {
                                    jenisKelamin = value;
                                  });
                                },
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  actions: < Widget > [
                    FlatButton(
                      onPressed: () {
                        if (_formTambahKeluarga.currentState.validate()) {
                          tambahKeluarga();
                        }
                      },
                      child: Text('Save'),
                    ),
                    FlatButton(
                      onPressed: () => Navigator.pop(context),
                      child: Text('Cancel'),
                    )
                  ],
                );
              }
            );
          },
          label: Text('Tambah'),
          icon: Icon(Icons.person_add),
          backgroundColor: Colors.pink,
        ),
      ),
      onWillPop: () {
        _tombolKembaliDitekan();
        return;
      }
    );

  }

  _displayNotifikasi(BuildContext context, _scaffoldKey, String pesan) {
    final snackBar = SnackBar(content: Text(pesan));
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }

  Future ambilDataKeluarga() async {
    http.Response hasilAmbilDataKeluarga = await http.post(
      Uri.encodeFull("${baseurl}keluarga/getKeluarga"), headers: {
        "Accept": "application/json"
      },
      body: {
        "user_id": userId.toString(),
      }
    );
    this.setState(() {
      Map < String, dynamic > dataKeluargaDecode = json.decode(hasilAmbilDataKeluarga.body);
      dataKeluarga = dataKeluargaDecode['data']['keluarga'];
    });
  }

  Future tambahKeluarga() async {
    if (tanggalLahir.value.text.toString() == '') {
      _displayNotifikasi(context, scaffoldKeluargaUpdate, 'Tanggal lahir tidak boleh kosong');
      Navigator.pop(context);
    } else if (jenisKelamin.toString() == 'null') {
      _displayNotifikasi(context, scaffoldKeluargaUpdate, 'Jenis kelamin tidak boleh kosong');
      Navigator.pop(context);
    } else {
      final response = await http.post("${baseurl}keluarga/tambahKeluarga", body: {
        "user_id": userId.toString(),
        "keluarga_nama": keluargaNama.value.text.toString(),
        "keluarga_jenis_kelamin": jenisKelamin.toString(),
        "keluarga_tgl_lahir": tanggalLahir.value.text.toString()
      });
      setState(() {
        var convertDataToJson = json.decode(response.body);
        Navigator.pop(context);
        if (convertDataToJson['status'] == 200) {
          _displayNotifikasi(context, scaffoldKeluargaUpdate, 'Data kerluarga berhasil ditambahkan');
        } else {
          _displayNotifikasi(context, scaffoldKeluargaUpdate, 'Terjadi kesalahan');
        }
      });
      _formTambahKeluarga.currentState.reset();
      keluargaNama.clear();
      tanggalLahir.clear();
    }
    ambilDataKeluarga();
  }

  _hapusKeluarga(keluargaId) async {
    final response =
      await http.post("${baseurl}keluarga/hapusKeluarga", body: {
        "keluarga_id": keluargaId.toString(),
      });
    setState(() {
      var convertDataToJson = json.decode(response.body);
      if (convertDataToJson['status'] == 200) {
        _displayNotifikasi(context, scaffoldKeluargaUpdate, 'Data keluarga berhasil dihapus');
        ambilDataKeluarga();
      } else {
        _displayNotifikasi(context, scaffoldKeluargaUpdate, 'Terjadi kesalahan');
      }
    });
  }

  @override
  // ignore: must_call_super
  void initState() {
    ambilDataKeluarga();
  }

  Future < void > _tombolKembaliDitekan() async {
    Navigator.pop(context, true);
    Navigator.pop(context, true);
    var route = new MaterialPageRoute(
      builder: (BuildContext context) => new TreatmentKategori(
        userId: userId,
        kembali: 2,
      ),
    );
    Navigator.of(context).push(route);
  }

}