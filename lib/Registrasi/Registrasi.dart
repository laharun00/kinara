import 'package:flutter/services.dart';
import 'package:kinara/host/Url.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'dart:convert';
import 'package:kinara/main.dart';
import 'package:http/http.dart'
as http;

void main() {
  runApp(MaterialApp(
    title: 'Registrasi',
    home: Registrasi(),
  ));
}

class Registrasi extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.3,
        centerTitle: true,
        title: const Text('Pendaftaran Kinara', style: TextStyle(color: Colors.black)),
          backgroundColor: Colors.white,
          leading: IconButton(
            color: Colors.blue[700],
            icon: Icon(Icons.arrow_back),
            onPressed: () => Navigator.pop(context, false)
          ),
      ),
      body: FormPendaftaran(),
    );
  }
}

class FormPendaftaran extends StatefulWidget {
  @override
  FormPendaftaranState createState() {
    return FormPendaftaranState();
  }
}

class FormPendaftaranState extends State < FormPendaftaran > {
  var _lihatPass = true;
  var _lihatPassKonf = true;
  // var _tombolDaftar = false;
  var res;
  var pesan;
  var _nama = new TextEditingController();
  var _email = new TextEditingController();
  var _password = new TextEditingController();
  var _notlp = new TextEditingController();
  var _alamat = new TextEditingController();
  var _passwordkonf = new TextEditingController();
  final _keyFormRegistrasi = GlobalKey < FormState > ();

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(0.0),
          boxShadow: [
            BoxShadow(
              color: Colors.black12,
              offset: Offset(0.0, 15.0),
              blurRadius: 15.0),
            BoxShadow(
              color: Colors.black12,
              offset: Offset(0.0, 15.0),
              blurRadius: 10.0)
          ]),
        child: Padding(
          padding: EdgeInsets.only(left: 16.0, right: 16.0, top: 00.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: < Widget > [
              Form(
                key: _keyFormRegistrasi,
                child: Column(
                  children: [
                    TextFormField(
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Nama tidak boleh kosong';
                        }
                        return null;
                      },
                      textCapitalization: TextCapitalization.words,
                      decoration: InputDecoration(
                        labelText: "Nama",
                        icon: Icon(Icons.person),
                        hintStyle: TextStyle(color: Colors.grey, fontSize: 16.0),
                      ),
                      controller: _nama,
                      maxLengthEnforced: false,
                    ),

                    SizedBox(height: ScreenUtil.getInstance().setHeight(30)),
                    TextFormField(
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Email tidak boleh kosong';
                        }
                        return null;
                      },
                      decoration: InputDecoration(
                        labelText: "Email",
                        hintStyle: TextStyle(color: Colors.grey, fontSize: 16.0),
                        icon: Icon(Icons.email),
                      ),
                      keyboardType: TextInputType.emailAddress,
                      controller: _email,
                      maxLengthEnforced: false,
                    ),

                    SizedBox(height: ScreenUtil.getInstance().setHeight(30)),
                    TextFormField(
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Nomor Telepon tidak boleh kosong';
                        }
                        return null;
                      },
                      decoration: InputDecoration(
                        labelText: "Nomor Telepon",
                        hintStyle: TextStyle(color: Colors.grey, fontSize: 16.0),
                        icon: Icon(Icons.phone),
                      ),
                      keyboardType: TextInputType.phone,
                      controller: _notlp,
                    ),

                    SizedBox(height: ScreenUtil.getInstance().setHeight(30)),
                    TextFormField(
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Alamat tidak boleh kosong';
                        }
                        return null;
                      },
                      decoration: InputDecoration(
                        labelText: "Alamat",
                        hintStyle: TextStyle(color: Colors.grey, fontSize: 16.0),
                        icon: Icon(Icons.map),
                      ),
                      keyboardType: TextInputType.multiline,
                      maxLines: 4,
                      textCapitalization: TextCapitalization.words,
                      controller: _alamat,
                    ),

                    SizedBox(height: ScreenUtil.getInstance().setHeight(30)),
                    TextFormField(
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Password tidak boleh kosong';
                        }
                        return null;
                      },
                      decoration: InputDecoration(
                        labelText: "Password",
                        hintStyle: TextStyle(color: Colors.grey, fontSize: 16.0),
                        icon: Icon(Icons.keyboard),
                        suffixIcon: IconButton(icon: Icon(Icons.remove_red_eye), onPressed: presLihatPass)
                      ),
                      controller: _password,
                      obscureText: _lihatPass,
                    ),

                    SizedBox(height: ScreenUtil.getInstance().setHeight(30)),
                    TextFormField(
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Password konfirmasi tidak boleh kosong';
                        }
                        return null;
                      },
                      decoration: InputDecoration(
                        labelText: "Password Konfirmasi",
                        hintStyle: TextStyle(color: Colors.grey, fontSize: 16.0),
                        icon: Icon(Icons.keyboard),
                        suffixIcon: IconButton(icon: Icon(Icons.remove_red_eye), onPressed: presLihatPassKonf)
                      ),
                      controller: _passwordkonf,
                      obscureText: _lihatPassKonf,
                    ),

                  ],
                ),
              ),

              SizedBox(height: ScreenUtil.getInstance().setHeight(30)),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 16.0),
                  child:
                  InkWell(
                    child: Container(
                      width: ScreenUtil.getInstance().setSp(300),
                      height: ScreenUtil.getInstance().setSp(100),
                      decoration: BoxDecoration(
                        gradient: LinearGradient(colors: [
                          Colors.blue[600],
                          Colors.blue[200]
                        ]),
                        borderRadius: BorderRadius.circular(6.0),
                        boxShadow: [
                          BoxShadow(
                            color: Color(0xFF6078ea).withOpacity(.3),
                            offset: Offset(0.0, 8.0),
                            blurRadius: 8.0)
                        ]),
                      child: Material(
                        color: Colors.transparent,
                        child: InkWell(
                          onTap: () {
                            if (_keyFormRegistrasi.currentState.validate()) {
                              _tambahUser(
                                _nama.text,
                                _email.text,
                                _notlp.text,
                                _alamat.text,
                                _password.text,
                                _passwordkonf.text,
                              );
                            }
                          },
                          child: Center(
                            child: Text(
                              "DAFTAR",
                              style: TextStyle(
                                color: Colors.white,
                                fontFamily: "Poppins-Bold",
                                fontSize: 10,
                                letterSpacing: 2.0
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void presLihatPass() {
    setState(() {
      _lihatPass = !_lihatPass;
    });
  }

  void presLihatPassKonf() {
    setState(() {
      _lihatPassKonf = !_lihatPassKonf;
    });
  }

  Future < void > _tambahUser(String _nama, String _email, String _notlp, String _alamat,
    String _password, String _passwordkonf) async {

    final response = await http.post("${baseurl}user/add", body: {
      "user_nama": _nama,
      "user_email": _email,
      "user_notlp": _notlp,
      "user_alamat": _alamat,
      "user_password": _password,
      "user_passwordkonf": _passwordkonf
    });
    setState(() {
      var convertDataToJson = json.decode(response.body);
      res = convertDataToJson['status'];
      pesan = convertDataToJson['message'];
    });
    onCreatedAccount(res, pesan);
  }

  void onCreatedAccount(res, pesan) {
    var alert = new AlertDialog(
      title: new Text('Hey'),
      content: new SingleChildScrollView(
        child: new ListBody(
          children: < Widget > [
            new Text(pesan),
          ],
        ),
      ),
      actions: < Widget > [
        new FlatButton(
          child: new Text('Ok'),
          onPressed: () {
            Navigator.of(context).pop();
            if (res == 200) {
              Navigator.push(context, MaterialPageRoute(builder: (context) => MyApp()));
            }
          },
        ),
      ],
    );
    showDialog(context: context, child: alert);
  }
}