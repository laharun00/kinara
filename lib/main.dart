import 'dart:ffi';
import 'package:geolocator/geolocator.dart';
import 'package:kinara/host/Url.dart';
import 'package:flutter/material.dart';
import 'package:kinara/Registrasi/Registrasi.dart';
import 'package:kinara/Treatment/TreatmentKategori.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart'
as http;

void main() => runApp(MaterialApp(
  home: MyApp(),
  debugShowCheckedModeBanner: false,
));

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => new _MyAppState();
}

class _MyAppState extends State < MyApp > {
  var res;
  var data;
  var _pseudoController = new TextEditingController();
  var _passwordController = new TextEditingController();
  var _lihatPassword = true;

  Widget build(BuildContext context) {
    cekGPS();
    ScreenUtil.instance = ScreenUtil.getInstance()..init(context);
    ScreenUtil.instance = ScreenUtil(width: 750, height: 1334, allowFontScaling: true);

    // _passwordController.text = 'harun';
    // _pseudoController.text = 'laharun00@gmail.com';

    return new Scaffold(
      backgroundColor: Colors.white,
      resizeToAvoidBottomPadding: true,
      body: Stack(
        fit: StackFit.expand,
        children: < Widget > [
          Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: < Widget > [
              Padding(
                padding: EdgeInsets.only(top: 20.0),
                child: Image.asset("assets/images/image_01.png"),
              ),
              Expanded(
                child: Container(),
              ),
            ],
          ),
          SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.only(left: 20.0, right: 20.0, top: 55.0),
              child: Column(
                children: < Widget > [
                  Row(
                    children: < Widget > [
                      Image.asset('assets/images/kinara-logo.png',
                        width: ScreenUtil.getInstance().setWidth(150),
                        height: ScreenUtil.getInstance().setHeight(150),
                      ),
                    ],
                  ),

                  SizedBox(height: ScreenUtil.getInstance().setHeight(300)),
                  Container(
                    width: double.infinity,
                    height: ScreenUtil.getInstance().setHeight(600),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(0.0),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black12,
                          offset: Offset(0.0, 15.0),
                          blurRadius: 15.0),
                        BoxShadow(
                          color: Colors.black12,
                          offset: Offset(0.0, 15.0),
                          blurRadius: 10.0)
                      ]),
                    child: Padding(
                      padding:
                      EdgeInsets.only(left: 16.0, right: 16.0, top: 16.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: < Widget > [
                          Text("Login",
                            style: TextStyle(
                              fontSize: ScreenUtil.getInstance().setSp(45),
                              fontFamily: "Poppins-Bold",
                              letterSpacing: .6)),

                          SizedBox(
                            height: ScreenUtil.getInstance().setHeight(30),
                          ),
                          Text("Email",
                            style: TextStyle(
                              fontFamily: "Poppins-Medium",
                              fontSize:
                              ScreenUtil.getInstance().setSp(26))),
                          TextField(
                            decoration: InputDecoration(
                              hintText: "Email",
                              hintStyle:
                              TextStyle(color: Colors.grey, fontSize: 12.0),
                            ),
                            keyboardType: TextInputType.emailAddress,
                            controller: _pseudoController,
                            maxLengthEnforced: false,
                          ),

                          SizedBox(
                            height: ScreenUtil.getInstance().setHeight(30),
                          ),
                          Text("Password",
                            style: TextStyle(
                              fontFamily: "Poppins-Medium",
                              fontSize:
                              ScreenUtil.getInstance().setSp(26))),
                          TextField(
                            decoration: InputDecoration(
                              hintText: "password",
                              hintStyle: TextStyle(
                                color: Colors.grey, fontSize: 12.0
                              ),
                              suffixIcon: IconButton(icon: Icon(Icons.remove_red_eye), onPressed: presLihatPass)
                            ),
                            obscureText: _lihatPassword,
                            controller: _passwordController,
                          ),

                          SizedBox(
                            height: ScreenUtil.getInstance().setHeight(35),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: < Widget > [
                              Text(
                                "Forgot password ?",
                                style: TextStyle(
                                  color: Colors.blue,
                                  fontFamily: "Poppins-Medium",
                                  fontSize: ScreenUtil.getInstance().setSp(30),
                                ),
                              )
                            ],
                          )
                        ],
                      ),
                    ),
                  ),

                  SizedBox(height: ScreenUtil.getInstance().setHeight(40)),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: < Widget > [
                      InkWell(
                        child: Container(
                          width: ScreenUtil.getInstance().setSp(300),
                          height: ScreenUtil.getInstance().setSp(100),
                          decoration: BoxDecoration(
                            gradient: LinearGradient(colors: [
                              Colors.blue[600],
                              Colors.blue[200]
                            ]),
                            borderRadius: BorderRadius.circular(6.0),
                            boxShadow: [
                              BoxShadow(
                                color: Color(0xFF6078ea).withOpacity(.3),
                                offset: Offset(0.0, 8.0),
                                blurRadius: 8.0)
                            ]),
                          child: Material(
                            color: Colors.transparent,
                            child: InkWell(
                              onTap: () {
                                openPageDaftar();
                              },
                              child: Center(
                                child: Text(
                                  "DAFTAR",
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontFamily: "Poppins-Bold",
                                    fontSize: 10,
                                    letterSpacing: 2.0),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                      InkWell(
                        child: Container(
                          width: ScreenUtil.getInstance().setSp(300),
                          height: ScreenUtil.getInstance().setSp(100),
                          decoration: BoxDecoration(
                            gradient: LinearGradient(colors: [
                              Colors.blue[600],
                              Colors.blue[200]
                            ]),
                            borderRadius: BorderRadius.circular(6.0),
                            boxShadow: [
                              BoxShadow(
                                color: Color(0xFF6078ea).withOpacity(.3),
                                offset: Offset(0.0, 8.0),
                                blurRadius: 8.0)
                            ]),
                          child: Material(
                            color: Colors.transparent,
                            child: InkWell(
                              onTap: () {
                                getLogin(
                                  _pseudoController.text,
                                  _passwordController.text,
                                );
                              },
                              child: Center(
                                child: Text(
                                  "MASUK",
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontFamily: "Poppins-Bold",
                                    fontSize: 10,
                                    letterSpacing: 2.0),
                                ),
                              ),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  void presLihatPass() {
    setState(() {
      _lihatPassword = !_lihatPassword;
    });
  }

  // ignore: missing_return
  Future < Void > getLogin(String pseudo, String password) async {
    final response = await http.post("${baseurl}auth", body: {
      "user_email": pseudo,
      "user_password": password,
    });
    setState(() {
      res = json.decode(response.body);
    });
    onLoginAccount(res, pseudo, password);
  }

  void openPageDaftar() {
    Navigator.push(context, MaterialPageRoute(builder: (context) => Registrasi()));
  }

  void onLoginAccount(res, pseudo, password) {
    var textAlert = '';
    if (res['status'] == 201) {
      textAlert = 'Sepertinya Email / Password Salah';
    }
    else if (res['status'] == 202) {
      textAlert = 'Akun Anda belum diverifikasi';
    }
    else {
      textAlert = res['data']['user']['user_nama'];
    }

    var alert = new AlertDialog(
      title: new Text('Hey'),
      content: new SingleChildScrollView(
        child: new ListBody(
          children: < Widget > [
            new Text(textAlert),
          ],
        ),
      ),
      actions: < Widget > [
        new FlatButton(
          child: new Text('Ok'),
          onPressed: () {
            Navigator.of(context).pop();
            if (res['status'] == 200) {
              var route = new MaterialPageRoute(
                builder: (BuildContext context) => new TreatmentKategori(
                  userId: res['data']['user']['user_id'],
                  userNama: res['data']['user']['user_nama'],
                  userPassword: res['data']['user']['user_password'],
                  userEmail: res['data']['user']['user_email'],
                  userNotlp: res['data']['user']['user_notlp'],
                  userAlamat: res['data']['user']['user_alamat'],
                  updatedAt: res['data']['user']['updated_at'],
                  createdAt: res['data']['user']['created_at']
                ),
              );
              Navigator.of(context).push(route);
            }
          },
        ),
      ],
    );
    showDialog(context: context, child: alert);
  }

  Future < Function > cekGPS() async {
    if (!(await Geolocator().isLocationServiceEnabled())) {
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text("Tidak bisa mendapatkan lokasi saat ini"),
              content:
                  const Text('Pastikan Anda mengaktifkan GPS dan coba lagi'),
              actions: <Widget>[
                FlatButton(
                  child: Text('Ok'),
                  onPressed: () {
                    Navigator.of(context, rootNavigator: true).pop();
                  },
                ),
              ],
            );
          },
        );
    }
    return null;
  }

}